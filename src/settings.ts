export default interface Settings {
  debug: boolean;
  gitlab: GitlabSettings;
  github: GithubSettings;
  usermap: {
    [key: string]: string;
  };
  projectmap: {
    [key: string]: string;
  };
  issueOffsets: {
    [key: string]: number;
  };
  conversion: {
    useLowerCaseLabels: boolean;
  };
  usePlaceholderIssuesForMissingIssues: boolean;
  useReplacementIssuesForCreationFails: boolean;
  useIssuesForAllMergeRequests: boolean;
  skipMatchingComments: string[];
  mergeRequests: {
    logFile: string;
    log: boolean;
  };
  s3?: S3Settings;
}

export interface GithubSettings {
  baseUrl?: string;
  owner: string;
  token: string;
  repo: string;
  timeout?: number;
  username?: string; // when is this set???
}

export interface GitlabSettings {
  url?: string;
  token: string;
  projectUrl: string;
  projectId: number;
}

export interface S3Settings {
  endpoint: string;
  accessKeyId: string;
  secretAccessKey: string;
  bucket: string;
}
